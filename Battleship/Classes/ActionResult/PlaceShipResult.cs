﻿using Battleship.Classes.ActionResult;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Classes
{
    public class PlaceShipResult : Result
    {
        public PlaceShipResult(bool WasPlaced)
        {
            this._WasPlaced = WasPlaced;
        }
        private bool _WasPlaced;
        public bool WasPlaced
        {
            get
            {
                return _WasPlaced;
            }
        }

    }
}
