﻿using Battleship.Classes.ActionResult;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Classes
{
    public class AttackResult : Result
    {
        public AttackResult (bool WasHit, bool WasKilled)
        {
            this._WasHit = WasHit;
            this._WasKilled = WasKilled;
        }
        private bool _WasHit;
        public bool WasHit
        {
            get
            {
                return _WasHit;
            }
        }

        private bool _WasKilled;
        public bool WasKilled
        {
            get
            {
                return _WasKilled;
            }
        }
    }
}
