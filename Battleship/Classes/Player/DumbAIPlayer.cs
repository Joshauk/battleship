﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Battleship.Classes.Player
{
    public class DumbAIPlayer : IPlayer
    {
        int AttackCount = 0;
        int ShipCount = 0;
        public Point RequestPlaceShip(Ships Ship)
        {
            return new Point(0, ShipCount++);
        }

        public Point RequestAttack()
        {
            Point AttackPoint = new Point(AttackCount % 10, AttackCount / 10); //Hard coded for board of 10 width
            AttackCount++;
            return AttackPoint;
        }

        public void ShipPlacementResult(bool Placed)
        {

        }

        public void AttackResult(bool Hit, bool Killed)
        {

        }

        public void GameOver()
        {

        }

    }
}
