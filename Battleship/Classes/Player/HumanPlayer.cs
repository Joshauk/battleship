﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Battleship.Classes.Player
{
    public class HumanPlayer : IPlayer
    {
        public Point RequestPlaceShip(Ships Ship)
        {
            try
            {
                Console.WriteLine("Place " + Ship.ToString());
                Console.Write("X:");
                String XPos = Console.ReadLine();
                Console.Write("Y:");
                String YPos = Console.ReadLine();
                int X = int.Parse(XPos);
                int Y = int.Parse(YPos);
                return new Point(X, Y);
            }
            catch (FormatException Ex)
            {
                Console.WriteLine("Invalid Coordinates");
                return RequestPlaceShip(Ship);
            }
        }

        public Point RequestAttack()
        {
            try
            {
                Console.WriteLine("Attack");
                Console.Write("X:");
                String XPos = Console.ReadLine();
                Console.Write("Y:");
                String YPos = Console.ReadLine();
                int X = int.Parse(XPos);
                int Y = int.Parse(YPos);
                return new Point(X, Y);
            }
            catch (FormatException Ex)
            {
                Console.WriteLine("Invalid Coordinates");
                return RequestAttack();
            }
        }

        public void ShipPlacementResult(bool Placed)
        {
            if (Placed == true)
                Console.WriteLine("Ship Placed");
            else
                Console.WriteLine("Ship Not Placed");
        }

        public void AttackResult(bool Hit, bool Killed)
        {
            if (Hit && Killed)
                Console.WriteLine("Ship Killed");
            else if (Hit)
                Console.WriteLine("Hit");
            else
                Console.WriteLine("Miss");
        }

        public void GameOver()
        {
            Console.WriteLine("Game Over");
        }
        //public String RequestAttack();
        //public PlaceShipResult PlaceShipUpdate();
        //public AttackResult PlaceShipUpdate();
    }
}
