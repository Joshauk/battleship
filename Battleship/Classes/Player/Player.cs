﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Battleship.Classes.Player
{
    public interface IPlayer
    {
        Point RequestPlaceShip(Ships Ship);
        Point RequestAttack();
        void ShipPlacementResult(bool Placed);
        void AttackResult(bool Hit, bool Killed);
        void GameOver();
    }
}
