﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Battleship.Classes
{
    public enum Orientations { Horizontal, Vertical};
    

    public class PlayerBoard
    {
        int Height = 10;
        int Width = 10;
        private bool Started = false;
        List<Ships> UnplacedShips = new List<Ships>();
        List<Ships> PlacedShips = new List<Ships>();
        Dictionary<ShipInfo, Position> ShipPositions = new Dictionary<ShipInfo, Position>();
        public PlayerBoard()
        {
            UnplacedShips = Enum.GetValues(typeof(Ships)).Cast<Ships>().ToList();
        }

        public void Start()
        {
            Started = true;
        }

        public bool HasStarted()
        {
            return Started;
        }

        public void PlaceShip (Ships ShipType, Position Pos)
        {
            if (ValidShipPlacement(ShipType, Pos))
            {
                PlacedShips.Add(ShipType);
                UnplacedShips.Remove(ShipType);
                ShipInfo ShifInfo = new ShipInfo(ShipType);
                ShipPositions.Add(ShifInfo, Pos);
            }
        }

        private bool ValidShipPlacement(Ships ShipType, Position Pos)
        {
            if (PlacementOutOfBounds(ShipType, Pos) == false && ShipPlacementOverlap(ShipType, Pos) == false)
                return true;
            else
                return false;
        }

        private bool PlacementOutOfBounds(Ships ShipType, Position Pos)
        {
            int XPos;
            int YPos;
            for (int i = 0; i < (int) ShipType; ++i)
            {
                if (Pos.Orientation == Orientations.Horizontal)
                {
                    XPos = Pos.xPos + i;
                    YPos = Pos.yPos;
                }
                else
                {
                    XPos = Pos.xPos;
                    YPos = Pos.yPos + i;
                }
                if (XPos < 0 || XPos > Width - 1)
                    return true;
                if (YPos < 0 || YPos > Height - 1)
                    return true;
                
            }
            return false;
        }

        private bool ShipPlacementOverlap(Ships PlacedShipType, Position PlacedPos)
        {
            bool[,] ShipGridPositions = new bool[10, 10];
            int XPos;
            int YPos;
            foreach (KeyValuePair<ShipInfo, Position> ExistingShip in ShipPositions)
            {
                Position Pos = ExistingShip.Value;
                Ships ShipType = ExistingShip.Key.Ship;
                for (int i = 0; i < (int)ShipType; ++i)
                {
                    if (Pos.Orientation == Orientations.Horizontal)
                    {
                        XPos = Pos.xPos + i;
                        YPos = Pos.yPos;
                    }
                    else
                    {
                        XPos = Pos.xPos;
                        YPos = Pos.yPos + i;
                    }
                    ShipGridPositions[XPos, YPos] = true;
                }
            }

            for (int i = 0; i < (int)PlacedShipType; ++i)
            {
                if (PlacedPos.Orientation == Orientations.Horizontal)
                {
                    XPos = PlacedPos.xPos + i;
                    YPos = PlacedPos.yPos;
                }
                else
                {
                    XPos = PlacedPos.xPos;
                    YPos = PlacedPos.yPos + i;
                }
                if (ShipGridPositions[XPos, YPos] == true)
                    return true;
            }
            return false;
        }

        public bool ShipPlaced(Ships Ship)
        {
            return PlacedShips.Contains(Ship);
        }

        public Position GetShipPosition(Ships Ship)
        {
            return ShipPositions.Where(x=>x.Key.Ship == Ship).FirstOrDefault().Value;
        }

        public AttackResult Attack(int xPos, int yPos)
        {
            foreach (KeyValuePair<ShipInfo, Position> ShipPos in ShipPositions)
            {
                

                bool ShipWasHit = TryHitShip(ShipPos.Key, ShipPos.Value, xPos, yPos);
                if (ShipWasHit)
                    return new AttackResult(true, ShipPos.Key.IsSunk());
                
            }
            return new AttackResult(false, false);
        }

        private bool TryHitShip(ShipInfo ShipInfo, Position Pos, int AttackX, int AttackY)
        {
            int Length = (int)ShipInfo.Ship;
            int xLength = 0;
            int yLength = 0;
            if (Pos.Orientation == Orientations.Horizontal)
                xLength = Length - 1;
            else
                yLength = Length - 1;

            int ShipXStart = Pos.xPos;
            int ShipXEnd = Pos.xPos + xLength;
            int ShipYStart = Pos.yPos;
            int ShipYEnd = Pos.yPos + yLength;


            if (AttackX >= ShipXStart && AttackX <= ShipXEnd && AttackY >= ShipYStart && AttackY <= ShipYEnd)
            {
                int HitPos;
                if (Pos.Orientation == Orientations.Vertical)
                    HitPos = AttackY - ShipYStart;
                else
                    HitPos = AttackX - ShipXStart;
                ShipInfo.Hit(HitPos);
                return true;
            }
            else
                return false;


        }

        public Ships? GetNextUnplacedShip()
        {
            if (UnplacedShips.Count() > 0)
                return UnplacedShips.First();
            else
                return null;
        }

        public bool AllShipsSunk()
        {
            if (ShipPositions.Keys.Where(x => x.IsSunk() == false).Count() == 0)
                return true;
            else
                return false;
        }
    }
}
