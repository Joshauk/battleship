﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Battleship.Classes
{
    public enum Ships { Cruiser = 3, Submarine = 4 };

    public class ShipInfo
    {
        private bool[] Damage;
        private Ships _Ship;
        public Ships Ship
        {
            get
            {
                return _Ship;
            }
        }
        public ShipInfo(Ships Ship)
        {
            this._Ship = Ship;
            this.Damage = new bool[(int)Ship]; //Ships enum cast to an int of their length
        }

        public void Hit(int Pos)
        {
            Damage[Pos] = true;
        }

        public bool IsSunk()
        {
            return Damage.ToList().Where(x=> x == false).Count() == 0; //if all positions are hit, the ship is sunk
        }
    }
}
