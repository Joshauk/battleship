﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Classes
{
    public class Position
    {
        // x and y are starting position
        // Then the ship either goes right or down
        public Position(int xPos, int yPos, Orientations Orientation)
        {
            this._xPos = xPos;
            this._yPos = yPos;
            this._Orientation = Orientation;

        }

        public override bool Equals(object other)
        {
            if (other.GetType() != this.GetType())
                return false;
            Position OtherPos = (Position)other;
            return OtherPos.xPos == this.xPos && OtherPos.yPos == this.yPos && OtherPos.Orientation == this.Orientation;
        }

        public override string ToString()
        {
            return "x:" + xPos + ", y:" + yPos + " Orientation:" + Orientation;
        }
        private int _xPos;
        public int xPos
        {
            get
            {
                return this._xPos;

            }
        }

        private int _yPos;
        public int yPos
        {
            get
            {
                return this._yPos;

            }
        }

        private Orientations _Orientation;
        public Orientations Orientation
        {
            get
            {
                return this._Orientation;

            }
        }

    }
}
