﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Battleship.Classes.Player;
using System.Drawing;

namespace Battleship.Classes
{
   
    public class BattleshipGame
    {
        enum GameStates { PlaceShips, Attack, GameOver};
        GameStates CurrentState = GameStates.PlaceShips;
        IPlayer Player1;
        PlayerBoard Board;
        List<Ships> PlacedShips = new List<Ships>();
        Dictionary<ShipInfo, Position> ShipPositions = new Dictionary<ShipInfo, Position>();
        public BattleshipGame(IPlayer Player1)
        {
            this.Player1 = Player1;
            Board = new PlayerBoard();
        }

        public void Start()
        {
            RunGame();
            
        }

        private void RunGame()
        {
            while (CurrentState != GameStates.GameOver)
            {
                if (CurrentState == GameStates.PlaceShips)
                    PlaceShipStep();
                else if (CurrentState == GameStates.Attack)
                    AttackStep();
            }
            Player1.GameOver();
        }

        private void PlaceShipStep()
        {
            Ships? ShipOrNull = Board.GetNextUnplacedShip();
            if (ShipOrNull == null)
                throw new Exception("No Ship Available To Place");
            Ships Ship = ShipOrNull.Value;
            Point Point = Player1.RequestPlaceShip(Ship);
            Board.PlaceShip(Ship, new Position(Point.X, Point.Y, Orientations.Horizontal));
            if (Board.ShipPlaced(Ship))
            {
                Player1.ShipPlacementResult(true);
                if (Board.GetNextUnplacedShip() == null)
                    CurrentState = GameStates.Attack;
            }
            else
            {
                Player1.ShipPlacementResult(false);
            }
        }

        private void AttackStep()
        {
            Point Point = Player1.RequestAttack();
            AttackResult Result = Board.Attack(Point.X, Point.Y);

            Player1.AttackResult(Result.WasHit, Result.WasKilled);

            if (Board.AllShipsSunk() == true)
                CurrentState = GameStates.GameOver;
        }
    }
}
