﻿using Battleship.Classes;
using Battleship.Classes.Player;
using System;
using System.Drawing;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            IPlayer Player1 = new HumanPlayer();
            BattleshipGame Game = new BattleshipGame(Player1);
            Game.Start();
            //Point Result = Player1.RequestPlaceShip(Ships.Cruiser);
            //Result = Player1.RequestAttack();
            
        }
    }
}
