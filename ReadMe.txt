﻿-- Battleship by Joshua Kearns --

Single game board only, no second player currently available.

How to Play:
- Run 'Program' to play using console
- Place 2 ships. 
- Start attacking positions.
- Game over when all ships sunk

Coordinates:
 - Board is 10x10
 - x and y coordinates entered are one at a time using digits 0-9 
	- Both for ship placement and attacks
 - Ship placement currently only horizontal from given position (option for vertical placement available as a microtransaction)


Program Structure:
 - BattleshipGame tracks the phase of the game (Placement, Attack, Game Over)
	- It requests moves from the Player
	- It makes moves on PlayerBoard
 - Player
	- Has DumbAIPlayer and HumanPlayer implementations
	- DumbAIPlayer currently only used for testing
	- HumanPlayer enters moves into Console
- PlayerBoard
	- Tracks Ships on the Board
	- Tells Battleship game the results of an Attack or Ship Placement Attempt

