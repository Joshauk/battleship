using Battleship.Classes;
using Battleship.Classes.Player;
using NUnit.Framework;
using System;
using System.Drawing;

namespace Tests
{
    public class PlayerBoardTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestDoNotStartGame()
        {
            PlayerBoard Game = new PlayerBoard();
            Assert.False(Game.HasStarted());
        }

        [Test]
        public void TestStartGame()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Assert.True(Game.HasStarted());
        }

        [Test]
        public void TestPlaceShip()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position Pos = new Position(0, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.True(Game.ShipPlaced(Ships.Cruiser));
            Assert.AreEqual(Pos, Game.GetShipPosition(Ships.Cruiser));
        }

        [Test]
        public void TestPlaceMultipleShips()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Assert.False(Game.ShipPlaced(Ships.Cruiser));
            Assert.False(Game.ShipPlaced(Ships.Submarine));
            Position CruiserPos = new Position(0, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, CruiserPos);
            Position SubPos = new Position(0, 1, Orientations.Horizontal);
            Game.PlaceShip(Ships.Submarine, SubPos);
            Assert.True(Game.ShipPlaced(Ships.Cruiser));
            Assert.True(Game.ShipPlaced(Ships.Submarine));
            Assert.AreEqual(CruiserPos, Game.GetShipPosition(Ships.Cruiser));
            Assert.AreEqual(SubPos, Game.GetShipPosition(Ships.Submarine));
            Assert.AreNotEqual(SubPos, CruiserPos);
        }

        [Test]
        public void TestPlacedOutOfBounds()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position Pos = new Position(20, 20, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.False(Game.ShipPlaced(Ships.Cruiser));
            Pos = new Position(-5, -5, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.False(Game.ShipPlaced(Ships.Cruiser));

        }

        [Test]
        public void TestPlacedHorizontalBounds()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position Pos = new Position(7, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Submarine, Pos);
            Assert.False(Game.ShipPlaced(Ships.Submarine));
            Pos = new Position(7, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.True(Game.ShipPlaced(Ships.Cruiser));
        }

        [Test]
        public void TestPlacedVerticalBounds()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position Pos = new Position(0, 7, Orientations.Vertical);
            Game.PlaceShip(Ships.Submarine, Pos);
            Assert.False(Game.ShipPlaced(Ships.Submarine));
            Pos = new Position(0, 7, Orientations.Vertical);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.True(Game.ShipPlaced(Ships.Cruiser));
        }

        [Test]
        public void TestShipOverlap()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position Pos = new Position(0, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Submarine, Pos);
            Assert.True(Game.ShipPlaced(Ships.Submarine));
            Pos = new Position(0, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.False(Game.ShipPlaced(Ships.Cruiser));
        }

        [Test]
        public void TestShipOverlapDifferentAxis()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position Pos = new Position(1, 0, Orientations.Vertical);
            Game.PlaceShip(Ships.Submarine, Pos);
            Assert.True(Game.ShipPlaced(Ships.Submarine));
            Pos = new Position(0, 3, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, Pos);
            Assert.False(Game.ShipPlaced(Ships.Cruiser));
        }

        [Test]
        public void TestAttack()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position CruiserPos = new Position(0, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, CruiserPos);
            bool Result = Game.Attack(0, 0).WasHit;
            Assert.True(Result);
            Result = Game.Attack(5, 5).WasHit;
            Assert.False(Result);

        }

        [Test]
        public void TestAttackWidth()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position CruiserPos = new Position(0, 0, Orientations.Horizontal);
            Game.PlaceShip(Ships.Cruiser, CruiserPos);
            bool Result = Game.Attack(1, 0).WasHit;
            Assert.True(Result);
        }

        [Test]
        public void TestAttackHeight()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position CruiserPos = new Position(0, 0, Orientations.Vertical);
            Game.PlaceShip(Ships.Cruiser, CruiserPos);
            bool Result = Game.Attack(0, 1).WasHit;
            Assert.True(Result);
            Result = Game.Attack(1, 0).WasHit;
            Assert.False(Result);
        }

        [Test]
        public void TestKillShip()
        {
            PlayerBoard Game = new PlayerBoard();
            Game.Start();
            Position CruiserPos = new Position(0, 0, Orientations.Vertical);
            Game.PlaceShip(Ships.Cruiser, CruiserPos);
            Game.Attack(0, 0);
            AttackResult Res = Game.Attack(0, 1);
            Assert.False(Res.WasKilled);
            Res = Game.Attack(0, 2);
            Assert.True(Res.WasKilled);
        }

        [Test]
        public void TestGetUnplacedShip()
        {
            PlayerBoard Board = new PlayerBoard();
            Ships? Ship = Board.GetNextUnplacedShip();
            Assert.AreEqual(Ships.Cruiser, Ship);
            Board.PlaceShip(Ships.Cruiser, new Position(0,0, Orientations.Horizontal));
            Ship = Board.GetNextUnplacedShip();
            Assert.AreEqual(Ships.Submarine, Ship);
            Board.PlaceShip(Ships.Submarine, new Position(0,1, Orientations.Horizontal));
            Ship = Board.GetNextUnplacedShip();
            Assert.IsNull(Ship);
        }

        [Test]
        public void AllShipsSunk()
        {
            PlayerBoard Board = new PlayerBoard();
            Board.Start();
            Position CruiserPos = new Position(0, 0, Orientations.Vertical);
            Board.PlaceShip(Ships.Cruiser, CruiserPos);
            Board.Attack(0, 0);
            Board.Attack(0, 1);
            Assert.False(Board.AllShipsSunk());
            Board.Attack(0, 2);
            Assert.True(Board.AllShipsSunk());
        }


    }
}