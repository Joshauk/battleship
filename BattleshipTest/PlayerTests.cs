using Battleship.Classes;
using Battleship.Classes.Player;
using NUnit.Framework;
using System;
using System.Drawing;

namespace Tests
{
    public class PlayerTests
    {
        [SetUp]
        public void Setup()
        {
        }

       

        [Test]
        public void TestDumbAIPlaceShip()
        {
            IPlayer Player1 = new DumbAIPlayer();
            Point Result = Player1.RequestPlaceShip(Ships.Cruiser);
            Assert.AreEqual(new Point(0,0), Result);
        }

        [Test]
        public void TestDumbAIAttack()
        {
            IPlayer Player1 = new DumbAIPlayer();
            Point Result = Player1.RequestAttack();
            Assert.AreEqual(new Point(0,0), Result);
            Result = Player1.RequestAttack();
            Assert.AreEqual(new Point(1, 0), Result);
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Result = Player1.RequestAttack();
            Assert.AreEqual(new Point(4, 1), Result);

        }

        //[Test] //Can't test console input
        //public void TestHumanAIPlaceShip()
        //{
        //    Player Player1 = new HumanPlayer();
        //    String Result = Player1.RequestPlaceShip(Ships.Cruiser);
        //    Assert.AreEqual(2, Result.Length);
        //    Assert.AreEqual("C2", Result);
        //}

      


    }
}